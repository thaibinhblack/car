$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
           
            $('#return-to-top').fadeIn();
            
        } else {
            $('#return-to-top').fadeOut();
        }
    });

    $(document).on("click", "#return-to-top", function() {
        $("html, body").animate({scrollTop: 0}, 1200);
     
        return false;
    });

})