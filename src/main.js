import Vue from 'vue'
import App from './App.vue'
import router from './router'
Vue.config.productionTip = false
import './assets/style.css';
import './assets/fonts/all.min.js'
import './assets/fonts/all.min.css'
import './assets/bootstrap/bootstrap.min.css'
import './assets/custom.css'
import store from './store.js'
import './assets/js/scroll.js'
import './assets/main.css'
import VuejsPaginate from 'vuejs-paginate'
Vue.component('paginate', VuejsPaginate)

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
