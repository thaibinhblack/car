import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
const routes = [
    
    {
        path: '/',
        component: () => import('@/pages/main.vue')
    },
    {
        path: '',
        component: () => import('@/layouts/main.vue'),
        children: [
            {
                path: '/:data',
                component: () => import('@/pages/index.vue')
            }
        ]
    },
    
    
]

const router = new VueRouter({
    routes: routes,
    mode: 'history'
})

export default router;