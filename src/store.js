import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)
const state = {
    doamin: 'https://minarental.com/?CARID=',
    loading: '',
    data: [],
    content: '',
    features: [],
    pages: 1,
    page_active: 1,
    number_show: 10,
    search: [],
    total: 0,
    total_features: [],
    total_rideshares: [],
    total_makes: [],
    DataMain: [],
    sidebar: false,
    hide_rideshare: '',
    local: '',
    check_empty_rideshares: false,
    check_empty_models: false,
    check_empty_makes: false,
    content_carental: '',
    content_aribnb: '',
    check_search: false,
    content_portfolio: '',
    content_ventures: ''
}
const getters = {
    loading: state => {
        return state.loading
    },
    
    features: state => {
        return state.features
    },
    total_features: state => {
        state.total_features = []
        state.features.find((feature) => {
            var total = 0;
            state.data.filter((value,index,array) => {
                var elemt_check = value[16] ? value[16] : ''
                if((elemt_check.toUpperCase()).indexOf(feature[1].toUpperCase()) != -1)
                {
                    total = total + 1;
                }
            })
            state.total_features.push(total)
        })
        return state.total_features
    },
    total_rideshares: state => {
        state.total_rideshares = []
        var check = 0
        state.features.find((feature) => {
            var total = 0;
            
            if(feature[2])
            {
                state.data.filter((value,index,array) => {
                    var elemt_check = value[17] ? value[17] : ''
                    if((feature[2]).toUpperCase() == (elemt_check).toUpperCase() )
                    {
                        total = total + 1;
                        check = 1
                    }
                })
            }
            if(total != 0)
            {
                state.check_empty_rideshares = true
            }
            state.total_rideshares.push(total)
            if(check == 0)
            {
                state.hide_rideshare = 'hiden'
            }
        })
        return state.total_rideshares
    },
    total_makes: state => {
        state.total_makes = []
        state.features.find((feature) => {
            var total = 0;
            if(feature[3])
            {
                state.data.filter((value,index,array) => {
                    var elemt_check = value[10] ? value[10] : ''
                    if((feature[3]).toUpperCase() == (elemt_check).toUpperCase() )
                    {
                        total = total + 1;
                    }
                })
            }
            if(total != 0)
            {
                state.check_empty_makes = true
            }
            state.total_makes.push(total)
        })
        return state.total_makes
    },
    total_models: state => {
        state.total_models = []
        state.features.find((feature) => {
            var total = 0;
            if(feature[4])
            {
                state.data.filter((value,index,array) => {
                    var elemt_check = value[11] ? value[11] : ''
                    if((feature[4]).toUpperCase() == (elemt_check).toUpperCase() )
                    {
                        total = total + 1;
                    }
                })
            }
            if(total != 0)
            {
                state.check_empty_models = true
            }
            state.total_models.push(total)
        })
        return state.total_models
    },
    pages: state => {
        return state.pages
    },
    data: state => {
        var data = state.data
        var result = []
        var i = state.page_active == 1 ? 0 : (state.page_active - 1)*state.number_show;
        var j = i == 0 ?  state.number_show : i + state.number_show
        var result = (data).slice(i,j)
        return result
    },
    search: state => {
        var data = state.search
        var result = []
        var i = state.page_active == 1 ? 0 : (state.page_active - 1)*state.number_show;
        var j = i == 0 ?  state.number_show : i + state.number_show
        // var result = (data).slice(i,j)
       
        // return result
        
        for(i; i < (state.page_active*state.number_show); i++)
        {
            if(data[i] != undefined)
            {
                result.push(data[i])
            }
        }
       
        return result
        
    },
    total: state => {
        return state.total
    },
    DataMain: state => {
        return state.DataMain
    },
    sidebar: state => {
        return state.sidebar
    },
    hide_rideshare: state => {
        return state.hide_rideshare
    },
    local: state => {
        return state.local
    }
}
const mutations = {
    SET_DATA(state,data)
    {
        state.total = (data).length 
        state.data =data
      
    },
    SET_CONTENT(state,content)
    {
        state.content = content
    },
    SET_FEATURE(state,features)
    {
        state.features = features
    },
    SET_PAGE(state,pages)
    {
        var pages = parseInt(pages/state.number_show)
        state.pages = pages == 0 ? 1 : pages + 1;
    },
    FILTER_DATA(state, search)
    {
        state.page_active = 1;
        if(search == null)
        {
            var result = []
            result = state.data.slice(0,10)
            state.check_search = false
        }
       else
       {
        var result = []
        state.check_search = true
        state.data.forEach((value,index,array) => {
            var check = 0;
            (array[index]).find((element) => {
               if((element.toUpperCase()).indexOf(search.toUpperCase()) != -1)
               {    
                    check = 1;
               }    
            })
            if(check == 1)
            {
                result.push(array[index]) 
                check = 0;
            }
            
        })
        state.total = (result).length
        state.search = result
        state.pages = parseInt(result.length / state.number_show)
        state.pages =  state.pages == 0 ?  1: state.pages + 1;
        state.loading = ''
       }
    },
    FilterFeature(state,filter)
    {
        state.check_search = true
        state.loading = 'active'
        state.page_active = 1;
        var result = []
        var data =  [];
        data = state.data
        if((filter.features).length >= 1)
        { 
            var num_check = (filter.features).length;
            
            data.forEach((value,index,array) => {
                var check = 0;
                var check_array = 0;
                var elemt_check = value[16] ? value[16] : ''
                
                if(elemt_check != '')
                {
                    filter.features.find(element => {
                        if((elemt_check.toUpperCase()).indexOf(element.toUpperCase()) != -1)
                        {
                            check_array = check_array + 1;
                        }  
                    })
                }
                if(check_array == num_check)
                {
                    result.push(array[index]) 
                    check_array  = 0;
                }
              
               
            })
        }
        if((filter.rideshare).length >= 1)
        {
            var data_rideshare = []
            if(result.length > 0)
            {
                data_rideshare = result
            }
            else{
                data_rideshare = data
            }
            result = []
            var num_check = (filter.rideshare).length;
            data_rideshare.forEach((value,index,array) => {
                var check = 0;
                var check_array = 0;
                var elemt_check = value[17] ? value[17] : ''
                
                if(elemt_check != '')
                {
                    (filter.rideshare).find(element => {
                        if((elemt_check).toUpperCase() == (element).toUpperCase())
                        {
                            check_array = check_array + 1;
                        }  
                    })
                }
                if(check_array == num_check)
                {
                    check = 1
                    check_array  = 0;
                }
                if(check == 1)
                {
                    result.push(array[index]) 
                    
                    check = 0;
                }
            })
        }
        if((filter.makes).length >= 1)
        {
            var data_makes = []
            if(result.length > 0)
            {
                data_makes = result
            }
            else{
                data_makes = data
            }
            result = []
            var num_check = (filter.makes).length;
            data_makes.forEach((value,index,array) => {
                var check = 0;
                var check_array = 0;
                var elemt_check = value[10] ? value[10] : ''
                
                if(elemt_check != '')
                {
                    (filter.makes).find(element => {
                        if((elemt_check).toUpperCase() == (element).toUpperCase())
                        {
                            check_array = check_array + 1;
                        }  
                    })
                }
                if(check_array == 1)
                {
                    result.push(array[index]) 
                    check = 0;
                    check_array  = 0;
                }
            })
        }
        if((filter.models).length >= 1)
        {
            var data_model = []
            if(result.length > 0)
            {
                data_model = result
            }
            else{
                data_model = data
            }
            result = []
            var num_check = (filter.models).length;
            data_model.forEach((value,index,array) => {
                var check = 0;
                var check_array = 0;
                var elemt_check = value[11] ? value[11] : ''
                
                if(elemt_check != '')
                {
                    (filter.models).find(element => {
                        if((elemt_check).toUpperCase() == (element).toUpperCase())
                        {
                            check_array = check_array + 1;
                        }  
                    })
                }
                if(check_array == num_check)
                {
                    check = 1
                    check_array  = 0;
                }
                if(check == 1)
                {
                    result.push(array[index]) 
                    check = 0;
                }
            })
        }
        if((filter.features).length == 0 && (filter.rideshare).length == 0 && (filter.makes).length == 0 && (filter.models).length == 0)
        {
            result = state.data
        }
        state.total = (result).length
        if((result).length == 0)
        {
            result = []
        }
        state.search = result
        const pages = parseInt(result.length / state.number_show)
        state.pages = pages == 0 ? 1 : pages + 1;
        state.loading = ''
    },
    FilterRideShare(state,RideShare)
    {
        state.loading = 'active'
        var result = []
        if(RideShare.length >= 1)
        { 
            
            
        }
        else
        {
            result = state.data
        }
        state.total = (result).length
        state.search = result
        const pages = parseInt(result.length / state.number_show)
        state.pages = pages == 0 ? 1 : pages + 1;
        state.loading = ''
    },
    NEXT_PAGE(state,page)
    {
        state.loading = 'active'
        page = page == 0 ? 1 : page
        page = page > state.pages ? state.pages : page ;
        state.page_active = page
    },
    FILTER_CARDID(state,CARID)
    {
        state.loading = 'active'
        var result = []
        state.data.filter((value,index,array) => {
            if(value[1] == CARID)
            {
                result.push(array[index])
            }
        })
        state.total = (result).length
        state.search = result
        state.pages = parseInt(result.length / state.number_show)
        state.pages =  state.pages == 0 ?  1: state.pages + 1;
        state.loading = ''
    },
    SET_DATA_MAIN(state,data)
    {
        state.DataMain = data
        state.content_carental = data[0][8]
        state.content_aribnb = data[0][9]
        state.content_portfolio = data[0][14]
        state.content_ventures = data[0][19]
    },
    close_sidebar(state,boolean)
    {
        state.sidebar = boolean
    },
    SET_LOCAL(state,local)
    {
        state.local = local
    }
}
const actions = {
    fetchDataMain({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get("https://sheets.googleapis.com/v4/spreadsheets/1VFxCxki4C6yQbzp19e6wuU2hAWHydnSVgKfzdn3oTyo/values/Main!A2:V2000?key=AIzaSyAWOdwaX9RAeTLO9tqlGovFg-CWhGX4TDM").then((response) => {
                commit("SET_DATA_MAIN", response.data.values);  
                resolve(response)
            
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    apiData({commit},data)
    {
        return new Promise((resolve,reject) => {
          
            axios.get(`https://sheets.googleapis.com/v4/spreadsheets/1VFxCxki4C6yQbzp19e6wuU2hAWHydnSVgKfzdn3oTyo/values/${data}!A2:AD2000?key=AIzaSyAWOdwaX9RAeTLO9tqlGovFg-CWhGX4TDM`).then((response) => {
                commit("SET_DATA", response.data.values);
                commit("SET_PAGE",(response.data.values).length);
   
                resolve(response)
            
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    filterData({commit},search){
        commit("FILTER_DATA",search)
    },
    fetchSetting({commit},location)
    {
       return new Promise((resolve,reject) => {
            axios.get(`https://sheets.googleapis.com/v4/spreadsheets/1VFxCxki4C6yQbzp19e6wuU2hAWHydnSVgKfzdn3oTyo/values/setting_${location}!A2:O2000?key=AIzaSyAWOdwaX9RAeTLO9tqlGovFg-CWhGX4TDM`).then((response) => {
                commit("SET_CONTENT",response.data.values[0][0])
                commit("SET_LOCAL",response.data.values[0][5])
                commit("SET_FEATURE",response.data.values)
                resolve(response)
            })
            .catch((err) => {
                reject(err)
            })
       })
    },
    FilterFeature({commit},filter)
    {
        commit("FilterFeature",filter)
    }
}
export default new Vuex.Store({
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions
})