const actions =  {
    apiData({commit},data)
    {
        return new Promise((resolve,reject) => {
          
            axios.get(`https://sheets.googleapis.com/v4/spreadsheets/1VFxCxki4C6yQbzp19e6wuU2hAWHydnSVgKfzdn3oTyo/values/${data}!A2:V2000?key=AIzaSyAWOdwaX9RAeTLO9tqlGovFg-CWhGX4TDM`).then((response) => {
                commit("SET_DATA", response.data.values);
                commit("SET_PAGE",(response.data.values).length);
   
                resolve(response)
            
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    filterData({commit},search){
        commit("FILTER_DATA",search)
    },
    fetchSetting({commit})
    {
       return new Promise((resolve,reject) => {
            axios.get('https://sheets.googleapis.com/v4/spreadsheets/1VFxCxki4C6yQbzp19e6wuU2hAWHydnSVgKfzdn3oTyo/values/Setting!A2:O2000?key=AIzaSyAWOdwaX9RAeTLO9tqlGovFg-CWhGX4TDM').then((response) => {
                commit("SET_CONTENT",response.data.values[0][0])
                commit("SET_FEATURE",response.data.values)
                resolve(response)
            })
            .catch((err) => {
                reject(err)
            })
       })
    },
    FilterFeature({commit},filter)
    {
        commit("FilterFeature",filter)
    }
}

export default actions;