$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#return-to-top').fadeIn();
        } else {
            $('#return-to-top').fadeOut();
        }
    });

    $('#return-to-top').click(function () {
        $("html, body").animate({scrollTop: 0}, 1200);
        return false;
    });

    const postDetails = document.querySelector(".content__right");
    const postSidebar = document.querySelector(".sidebar__left");
    const postSidebarContent = document.querySelector(".sidebar__left > div");

//1
    const controller = new ScrollMagic.Controller();

//2
    const scene = new ScrollMagic.Scene({
        triggerElement: postSidebar,
        triggerHook: 0,
        duration: getDuration
    }).addTo(controller);

//3
    if (window.matchMedia("(min-width: 768px)").matches) {
        scene.setPin(postSidebar, {pushFollowers: false});
    }

//4
    window.addEventListener("resize", () => {
        if (window.matchMedia("(min-width: 768px)").matches) {
            scene.setPin(postSidebar, {pushFollowers: false});
        } else {
            scene.removePin(postSidebar, true);
        }
    });

    function getDuration() {
        return postDetails.offsetHeight - postSidebarContent.offsetHeight;
    }
})